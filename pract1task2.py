import random
import os

def generate_mas(size):
    return sorted(random.sample(range(200), size))

def zn_search(mas, target):
    low = 0
    high = len(mas) - 1
    while low <= high:
        mid = (low + high) // 2
        if mas[mid] == target:
            return mid
        elif mas[mid] < target:
            low = mid + 1
        else:
            high = mid - 1
    return -1

if __name__ == "__main__":
    try:
        target_value = int(os.getenv("TARGET_VALUE"))
    except ValueError:
        print("Введите целое число")
        exit(1)

    mas_size = 100
    mas_sorted = generate_mas(mas_size)

    result = zn_search(mas_sorted, target_value)

    if result != -1:
        print(f"Значение {target_value} найдено в массиве и занимает позицию {result}")
    else:
        print(f"Значение {target_value} не найдено в массиве")
