FROM python:3.9
WORKDIR /app
COPY pract1task2.py /app/
CMD ["python", "pract1task2.py", "${TARGET_VALUE}"]